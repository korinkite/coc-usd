import { languages, commands, ExtensionContext, workspace } from '/home/selecaoone/repositories/coc.nvim/src/index'
import { Completer } from "./coc-usd/completer"


export function activate(context: ExtensionContext) {
	const priority = workspace.getConfiguration('usd').get<number>('priority')

	let completion = languages.registerCompletionItemProvider(
		'usd',
		'usd',
		['usd', 'usda'],
		new Completer(),
		['(', ')', '{', '}'],
		priority
	)

	context.subscriptions.push(completion)
}
